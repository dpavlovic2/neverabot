import {Config} from './config/config.js';
import {WebServer} from './components/webServer.js';
import {CronJobFunc} from './components/cronJob.js';

//applicationMode switch that is based of configuration object in config.js file
Config.inApplicationMode ? 
    WebServer() : CronJobFunc();