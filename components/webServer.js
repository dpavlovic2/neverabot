import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
const path = require('path');
import async from 'async';
import pdf from 'html-pdf';
import webshot from 'webshot';
import fs from 'fs';
import { Config } from '../config/config.js';
import { Signee, ExcludedFields } from '../models/signee.js';

//webserver function used in root app.js file
export const WebServer = () => {
    const server = express();

    //mixin - see configuration variable below
    Object.assign(server, configuration);

    server.configureServer()
        .configureRoutes()
        .start();
};

let configuration = {
    //initial server configuration
    configureServer: function () {
        this.set('view engine', 'ejs');
        this.set('views', path.join(__dirname, '../views'));

        this.use('/static', express.static(path.join(__dirname, '../static')));
        this.use(bodyParser.urlencoded({ extended: false }));
        this.use(bodyParser.json());
        mongoose.connect(Config.databaseConnection);
        return this;
    },
    //all routes are configured here and root folder for views is views folder
    configureRoutes: function () {
        //route that renders home page/root of application
        this.get('/', (req, res) => req.query.error ? res.render('index.ejs', { err: true }) : res.render('index.ejs', { err: false }));
        this.get('/about', (req, res) => res.render('about.ejs'));
        this.get('/whosigned', (req, res) => {
            async.waterfall([
                (cb) => {
                    Signee.aggregate(
                        { $match: { isPublic: true } },
                        { $group: { _id: '$city', count: { $sum: 1 } } },
                        { $limit: 3 }
                    ).exec((err, cities) => {
                        if (err) {
                            return cb(500, {
                                message: 'Error counting cities'
                            });
                        } else {
                            cb(null, cities.map(cityObj => cityObj._id));
                        }
                    });
                },
                (cities, cb) => {
                    Signee.find({})
                        .select('birthDate -_id')
                        .exec((err, birthDates) => {
                            if (err) {
                                return cb(500, {
                                    message: 'Error while calculating average birthday'
                                });
                            } else {
                                const dates = birthDates.map(bDay => helpers.getAge(bDay.birthDate))
                                    .filter(age => age > 0);
                                let sum = 0;
                                for (let i = 0; i < dates.length; i++) {
                                    sum += dates[i];
                                }
                                let average = 0;
                                if (sum > 0) {
                                    average = sum / dates.length;
                                }
                                cb(null, {
                                    locations: cities,
                                    averageAge: average
                                });
                            }
                        });
                },
                (data, cb) => res.render('whosigned.ejs', data)
            ], (statusCode, err) => res.status(statusCode).send(err));


        });

        this.get('/webpreview/:id', (req, res) => Signee.findOne({ _id: req.params.id }).exec((err, signee) => {
            if (err) {
                console.log('Something went wrong');
                res.redirect('/');
            } else {
                return res.render('preview.ejs', signee);
            }
        }));

        this.get('/print/:id', (req, res) => Signee.findOne({ _id: req.params.id }).exec((err, signee) => {
            if (err) {
                console.log('Something went wrong');
                res.redirect('/');
            } else {
                return res.render('print.ejs', signee);
            }
        }));

        this.get('/download/:id', (req, res) => {
            const id = req.params.id;

            async.waterfall([
                (cb) => {
                    Signee.findOne({ _id: req.params.id }).exec((err, signee) => {
                        if (err) {
                            return cb(500, {
                                message: 'Error while fetching signee (download)'
                            });
                        } else {
                            cb(null, signee);
                        }
                    });
                },
                (signee, cb) => res.app.render(Config.viewUsedToGeneratePDF, signee, (err, htmlStr) => cb(null, htmlStr)),
                (fileName, cb) => pdf.create(fileName, Config.pdfOpts).toStream((err, stream) => {
                    if (err) {
                        return cb(500, {
                            message: 'Error while generating PDF (download)'
                        });
                    } else {
                        res.setHeader('Content-disposition', `attachment; filename=${id}.pdf`);
                        res.contentType('application/pdf');

                        stream.on('data', data => res.write(data))
                            .on('end', () => res.end());
                    }
                })
            ], (statusCode, err) => res.status(statusCode).send(err));
        });

        //save to db, create pdf and return path to pdf (to open tab on client after ajax)
        this.post('/sign', (req, res) => {
            const payload = req.body;
            console.log(payload);
            if (payload.name && payload.surname && payload.city && payload.country && payload.birthDate && payload.signedInCity && payload['fake-captcha']) {
                payload.name = `${payload.name.charAt(0).toUpperCase()}${payload.name.slice(1)}`;
                payload.surname = `${payload.surname.charAt(0).toUpperCase()}${payload.surname.slice(1)}`;
                payload.isPublic = payload.isPublic ? true : false;
                payload.isPersisted = payload.isPersisted ? true : false;
                let modelData = {};

                Object.keys(payload).map(key => key)
                    .forEach(key => modelData[key] = payload[key]);

                async.waterfall([
                    //first we saved our data to the db
                    (cb) => new Signee(modelData).save((err, savedModel) => {
                        if (err) {
                            return cb(500, {
                                message: 'Error while saving data to the db!'
                            })
                        } else {
                            cb(null, savedModel);
                        }
                    }),
                    //next we create url to pdf preview that will be used by frontend code
                    (savedModel, cb) => {
                        if (!savedModel) {
                            return cb(500, {
                                message: 'Model not present'
                            });
                        } else {
                            cb(null, savedModel);
                        }
                    },
                    (model, cb) => {
                        return res.status(200).send({
                            preview: `/webpreview/${model._id}`
                        });
                    }
                ], (statusCode, data) => res.status(statusCode).send(data));
            } else {
                return res.status(500).send({
                    message: 'Model not present'
                });
            }
        });

        //sort signees by creation date or if isAplhabetical is true by their Name
        this.get('/signees/:isAlphabetical?', (req, res) => {
            const isAplhabetical = req.params.isAlphabetical,
                mapWithout = ExcludedFields.map(field => `-${field}`).join(' '),
                mapperFunction = x => {
                    if (x.isPublic) {
                        x.name = 'Anonymous';
                        x.surname = 'user';
                        return x;
                    } else {
                        return x;
                    }
                };

            if (isAplhabetical) {
                Signee.find({ isPersisted: true }).sort({ name: -1 })
                    .select(mapWithout)
                    .exec((err, signees) => {
                        if (err) {
                            return res.status(500).send({
                                message: 'Error while trying to get list of users'
                            });
                        } else {
                            return res.status(200).send(signees.map(mapperFunction));
                        }
                    })
            } else {
                Signee.find({ isPersisted: true }).sort({ createdDate: 1 })
                    .select(mapWithout)
                    .exec((err, signees) => {
                        if (err) {
                            return res.status(500).send({
                                message: 'Error while trying to get list of users'
                            });
                        } else {
                            return res.status(200).send(signees.map(mapperFunction));
                        }
                    });
            }
        });
        //statistics that depend on property sent to route (properties are ones defined on model and statistics is currently count only)
        this.get('/stats/:property', (req, res) => {
            const property = req.params.property,
                group = {};

            if (property) {
                group['_id'] = `$${property}`;
                Object.assign(group, { count: { $sum: 1 } });

                Signee.aggregate({
                    $group: group
                }, (err, result) => {
                    if (!err && result) {
                        return res.status(200).send(result);
                    }
                });
            } else {
                return res.status(500).send({
                    message: `Error while creating statistic from property ${property}`
                });
            }
        });

        return this;
    },
    //startup function for configuring port and similar
    start: function () {
        this.listen(Config.port,
            () => console.log(`App listening on port: ${Config.port}`));
    }
}

const helpers = {
    getAge: (dateString) => {
        const today = new Date();
        const birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
}