import express from 'express';
import mongoose from 'mongoose';
import async from 'async';
import pdf from 'html-pdf';
import { CronJob } from 'cron';
import { Config } from '../config/config.js';
import { Signee } from '../models/signee.js';

const server = express();

//cron job function used in root app.js file
export const CronJobFunc = () => {
    Object.assign(server, configuration);

    server.configureServer()
        .start();

    new CronJob(Config.cronjobInterval, configureJob).start();
};

//initial cron job configuration
let configuration = {
    configureServer: function () {
        this.set('view engine', 'ejs');
        mongoose.connect(Config.databaseConnection);
        return this;
    },
    start: function () {
        this.listen(Config.port,
            () => console.log(`Cronjob running on port: ${Config.port}`))
    }
}

//find all signees them for each create PDF document and updated record in database in order to skip next iteration of cronjob
let configureJob = () => Signee.find({ isPrinted: false }, (err, models) => {
    if (err) {
        console.log('Error while fetching signees from database (cronjob)!');
    } else {
        async.forEachLimit(models, models.length, (model, cb) => {
            async.waterfall([
                (cb) => {
                    server.render(Config.viewUsedToGeneratePDF, model, (err, html) => {
                        if (err) {
                            return cb('Error while rendering HTML content (cronjob)!');
                        } else {
                            cb(null, html);
                        }
                    });
                },
                (html, cb) => {
                    pdf.create(html, Config.pdfOpts).toFile(`${Config.pdfPath}${model._id}.pdf`, (err, res) => {
                        if (err) {
                            cb('Error while creating pdf file (cronjob)!');
                        } else {
                            cb(null);
                        }
                    });
                },
                (cb) => {
                    if (!model.isPersisted) {
                        Signee.find({ _id: model._id }).remove().exec(() => console.log(`Removed document: ${model._id}`));
                        console.log(`Pdf created: ${model._id}.pdf, model printed`);
                    } else {
                        Signee.findOneAndUpdate({ _id: model._id }, { isPrinted: true }, (err, updated) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(`Pdf created: ${model._id}.pdf, model printed`);
                            }
                        });
                    }
                }
            ], (err) => console.log(err));
        });
    }
});