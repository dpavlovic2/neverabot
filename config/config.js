//configuration object used inside of application
const path = require('path');

const Config = {
    port: 13646,
    pdfPath: './pdf/',
    cronjobInterval: '*/30 * * * * *',
    inApplicationMode: true,
    viewUsedToGeneratePDF: 'pdf.ejs',
    pdfOpts: {
        format: 'A4',
        base: `file:///${path.join(__dirname, '../static/').replace(/\\/g, '/')}`
    },
    databaseConnection: 'mongodb://neverabot:wT8Wqd#*@ds131878.mlab.com:31878/neverabot'
    //databaseConnection: 'mongodb://vas_admin:vas@ds035995.mongolab.com:35995/vas'

};

export { Config };