Neverabot - Info
======

Config
------

* config.js - initial app config (used to switch app in different modes)

Development
------

* npm start - script that watches all changes done to .js files and restarts the application

Tools needed
------
* node, npm/yarn - to run the application and manage packages
* editor
* git

### Deployment ###
To add a Node.js application’s bin directory to your PATH environment variable:

Switch to your Node.js application’s directory. Enter 
```
#!cmd

cd $HOME/webapps/neverabot/
```
where *neverabot* is the name of your Node.js application as it appears on the control panel


```
#!cmd

export PATH=$PWD/bin/:$PATH
```
to add the application’s **bin** directory to your **PATH** environment variable. For the remainder of the SSH session, you can run your application’s executables without entering a full path.th.

## Restarting the app ##


```
#!cmd

cd neverabot
pm2 delete all
pm2 start npm -- start
pm2 stop all
pm2 restart 0
```