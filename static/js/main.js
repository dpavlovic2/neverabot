$(function () {
    var signeesAlphabet = null;
    var signeesChrono = null;
    function search(nameKey, myArray) {
        if (nameKey !== "") {
            nameKey = nameKey.toLowerCase();

            var filtered = myArray.filter(function (x) {
                return x.name.toLowerCase().startsWith(nameKey)
                    || x.surname.toLowerCase().startsWith(nameKey)
            }).sort(function (a, b) {
                if (a.name.length < b.name.length) return -1;
                if (a.name.length > b.name.length) return 1;
                return 0;
            });

            return filtered;
        } else {
            return myArray;
        }
    }


    function getSigneesChrono() {
        $.get('/signees').then(
            function (resp) {
                $('.signees-chrono').empty();
                signeesAlphabet = resp;
                signeesChrono = resp;
                var signeesCount = resp.length + 1;
                $('#signees-count').text(signeesCount + ' signed so far');
                resp.forEach(function (item) {
                    $('.signees-chrono').append(
                        '<li>' + item.name + ' ' + item.surname + ', ' + item.city + '</li>'
                    )
                })
            },
            function (err) {
                $('.signees-chrono').empty().append(
                    '<li>Something went wrong</li>'
                )
            }
        )
    }

    function getSigneesAlphabet() {
        $.get('/signees/isAlphabetical').then(
            function (resp) {
                signeesAlphabet = resp;
                $('.signees-alphabet').empty();
                resp.forEach(function (item) {
                    $('.signees-alphabet').append(
                        '<li>' + item.name + ' ' + item.surname + ', ' + item.city + '</li>'
                    )
                })
            },
            function (err) {
                $('.signees-alphabet').empty().append(
                    '<li>Something went wrong</li>'
                )
            }
        )
    }

    if ('.signees') {
        if (!signeesChrono) {
            getSigneesChrono();
        } else {
            signeesChrono.forEach(function (item) {
                $('.signees-chrono').append(
                    '<li>' + item.name + ' ' + item.surname + '</li>'
                )
            });
        }
        if (!signeesChrono) {
            getSigneesAlphabet();
        } else {
            signeesAlphabet.forEach(function (item) {
                $('.signees-alphabet').append(
                    '<li>' + item.name + ' ' + item.surname + '</li>'
                )
            });
        }
        $(document).on('click', '#show-alphabet', function (e) {
            e.preventDefault();
            $('.signees-chrono').css({
                display: 'none',
            }).removeClass('active');
            $('.signees-alphabet').css({
                display: 'block',
            }).addClass('active');
        });
        $(document).on('click', '#show-chrono', function (e) {
            e.preventDefault();
            $('.signees-alphabet').css({
                display: 'none',
            }).removeClass('active');
            $('.signees-chrono').css({
                display: 'block',
            }).addClass('active');
        });
        $('#search').on('keyup', function (e) {
            if (signeesAlphabet) {

                var result = search($(this).val(), signeesAlphabet);

                $('.signees.active').empty();

                result.forEach(function (item) {
                    $('.signees.active').append(
                        '<li>' + item.name + ' ' + item.surname + ', ' + item.city + '</li>'
                    )
                });
            }
        })
    }
})