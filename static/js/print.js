$(function () {
    function validateForm(){
        var errors = [];
        var firstName = $('#first-name');
        var lastName = $('#last-name');
        var birthDay = $('#birthday');
        var birthMonth = $('#birthmonth');
        var birthYear = $('#birthyear');
        var city = $('#city');
        var country = $('#country');
        var locationInput = $('#location-input');

        $('input, select').removeClass('error');
        $('span, label').removeClass('error-text');
        $('header .alert').css({
            display: 'none',
        });
        $('.still-human').css({
            display: 'none',
        });
        if(!$('#not-bot').is(':checked')){
            errors.push('Captcha');
            $('.still-human').css({
                display: 'block',
            });
        }
        if(!firstName[0].value){
            firstName.addClass('error');
            errors.push('First Name');
        }
        if(!lastName[0].value){
            lastName.addClass('error');
            errors.push('Last Name');
        }
        if(!birthDay[0].value){
            birthDay.addClass('error');
            errors.push('Day of Birth');
        }
        if(!birthMonth[0].value){
            birthMonth.addClass('error');
            errors.push('Month of Birth');
        }
        if(!birthYear[0].value){
            birthYear.addClass('error');
            errors.push('Year of Birth');
        }
        if(!city[0].value){
            city.addClass('error');
            errors.push('City');
        }
        if(!country[0].value){
            country.addClass('error');
            errors.push('Country');
        }
        if(!locationInput[0].value){
            locationInput.addClass('error');
            errors.push('Location');
        }
        return errors;
    }
    $('#statement-form').on('submit', function (e) {
        e.preventDefault();
        var errors = validateForm();
        var alertFields = $('header .alert__fields');
        if(errors.length > 0){
            var indexOfCaptcha = errors.indexOf('Captcha');
            if (indexOfCaptcha > -1) {
                errors.splice(indexOfCaptcha, 1);
            }
            if(errors.length > 0){
                alertFields.empty().append(errors.join(', '));
                $('.alert').css({
                    display: 'block',
                });
            }
            $( ".logo" ).addClass( "logo-hover" );
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return;
        }
        var birthDay = $(this)[0][3].value + '/' + $(this)[0][2].value + '/' + $(this)[0][4].value;
        var serializedValue = $(this).serialize() + '&birthDate=' + birthDay;
        $.post('/sign', serializedValue).then(function (resp) {
            location.href = resp.preview;
            console.log('succ')
        }, function (err) {
            alert('Something went wrong. Please try again and check format of input values');
        });
    })
});