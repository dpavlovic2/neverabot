$(function(){
    var n = new Date();
    var y = n.getFullYear();
    var m = n.getMonth() + 1;
    var d = n.getDate();
    document.getElementById("date").innerHTML = d + "/" + m + "/" + y;

    $( ".logo" ).hover(
        function() {
            $( ".paper" ).addClass( "shadow-red" );
            $( this ).addClass( "logo-hover" );
        }, function() {
            $( ".paper" ).removeClass( "shadow-red" );
            $( this ).removeClass( "logo-hover" );
        }
    );
});

$('#is-persisted').click(function() {
  $('#is-public-checkbox')[this.checked ? "show" : "hide"]();
});