import mongoose from 'mongoose';

const Schema = mongoose.Schema,
    SigneeSchema = new Schema({
        name: String,
        surname: String,
        city: String,
        country: String,
        birthDate: Date,
        signedInCity: String,
        isPrinted: {
            type: Boolean,
            default: false
        },
        isPersisted: {
            type: Boolean,
            default: true
        },
        isPublic: {
            type: Boolean,
            default: false
        },
        createdDate: {
            type: Date,
            default: Date.now
        }
    },
        {
            collection: 'Signees'
        });

export const Signee = mongoose.model('Signee', SigneeSchema);
export const ExcludedFields = ['_id', 'createdDate', 'isPrinted', '__v'];
